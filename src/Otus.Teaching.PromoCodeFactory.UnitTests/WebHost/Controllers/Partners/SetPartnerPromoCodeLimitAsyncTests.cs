﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProviderMock;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        // Для фиксации текущей даты в тестах.
        private readonly DateTime NowDate = new DateTime(2020, 09, 07);

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();

            _currentDateTimeProviderMock = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            {
                _currentDateTimeProviderMock.Setup(p => p.CurrentDateTime).Returns(NowDate);
            }

            _partnersController = fixture
                .Build<PartnersController>()
                .OmitAutoProperties()
                .Create();
        }

        /// <summary>
        /// 1. Если партнер не найден, то также нужно выдать ошибку 404.
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenPartnerIsNotExist_ShouldReturnNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            var request = SetPartnerPromoCodeLimitRequestBuilder
                .CreateBaseSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400.
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenPartnerIsNotActive_ShouldReturnBadRequest()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().WithFalseActive();

            var request = SetPartnerPromoCodeLimitRequestBuilder
                .CreateBaseSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes.
        /// Если лимит закончился, то количество не обнуляется.
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenPartnerHasActiveLimit_ShouldResetIssuedPromoCodes()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner();

            var request = SetPartnerPromoCodeLimitRequestBuilder
                .CreateBaseSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes.
        /// Если лимит закончился, то количество не обнуляется.
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenPartnerHasNoActiveLimit_ShouldNotResetIssuedPromoCodes()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner()
                .WithEmptyPartnerLimits()
                .WithSingleIssuedPromoCode();

            var request = SetPartnerPromoCodeLimitRequestBuilder
                .CreateBaseSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(1);
        }

        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит.
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenSetLimit_ShouldResetPreviousLimit()
        {
            // Arrange
            var request = SetPartnerPromoCodeLimitRequestBuilder
                .CreateBaseSetPartnerPromoCodeLimitRequest();

            var partner = PartnerBuilder.CreateBasePartner();
            var partnerLimit = partner.PartnerLimits.First();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partnerLimit.CancelDate.Should().Be(NowDate);
        }

        /// <summary>
        /// 5. Лимит должен быть больше 0.
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenLimitIsNegative_ShouldReturnBadRequest()
        {
            // Arrange
            var request = SetPartnerPromoCodeLimitRequestBuilder
                .CreateBaseSetPartnerPromoCodeLimitRequest()
                .WithNegativeLimit();

            var partner = PartnerBuilder.CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 6. Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом).
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenSetLimit_ShouldAddNewLimit()
        {
            // Arrange
            var request = SetPartnerPromoCodeLimitRequestBuilder
                .CreateBaseSetPartnerPromoCodeLimitRequest();

            var partner = PartnerBuilder.CreateBasePartner();
            var partnerLimit = partner.PartnerLimits.First();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            _partnersRepositoryMock.Verify(p => p.UpdateAsync(partner), Times.Once);
        }
    }
}