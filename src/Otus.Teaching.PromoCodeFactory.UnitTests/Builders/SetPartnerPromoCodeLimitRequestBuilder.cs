﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class SetPartnerPromoCodeLimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateBaseSetPartnerPromoCodeLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.MaxValue,
                Limit = 200
            };
        }

        public static SetPartnerPromoCodeLimitRequest WithNegativeLimit(this SetPartnerPromoCodeLimitRequest request)
        {
            request.Limit = -1;
            return request;
        }
    }
}
