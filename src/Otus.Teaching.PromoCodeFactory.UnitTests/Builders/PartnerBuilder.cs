﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerBuilder
    {
        public static Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 09, 06),
                        EndDate = new DateTime(2020, 11, 06),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public static Partner WithEmptyPartnerLimits(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            return partner;
        }

        public static Partner WithSingleIssuedPromoCode(this Partner partner)
        {
            partner.NumberIssuedPromoCodes = 1;
            return partner;
        }

        public static Partner WithFalseActive(this Partner partner)
        {
            partner.IsActive = false;
            return partner;
        }
    }
}
