﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core
{
    public class CurrentDateTimeProvider : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}
