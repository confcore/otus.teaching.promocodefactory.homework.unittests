﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}
